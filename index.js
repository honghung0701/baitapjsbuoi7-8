var numberArray = [];
document.getElementById('addNumber').onclick = function() {
    var n = document.getElementById('number').value*1;
    numberArray.push(n);
    document.getElementById('array').innerHTML = numberArray;
}
// Bài 1:
document.getElementById('sum').onclick = function() {
    var sum = 0;
    for (var i = 0; i<=numberArray.length; i++) {
        if (numberArray[i] > 0) {
            sum+=numberArray[i];
        }
    }
    document.getElementById('result_1').innerHTML = `Tổng số dương: ` + sum;
}
// Bài 2:
document.getElementById('count').onclick = function() {
    var plusNumber = 0;
    for (var i = 0; i<=numberArray.length; i++) {
        if (numberArray[i] > 0) {
            plusNumber++;
        }
    }
    document.getElementById('result_2').innerHTML = `Số dương: ` +plusNumber;
}
// Bài 3:
document.getElementById('minNumber').onclick = function () {
    var min = numberArray[0];
    for (var i = 0; i<=numberArray.length; i++) {
        if (min > numberArray[i]) {
            min = numberArray[i];
        } else {
            min = min;
        }
    }
    document.getElementById('result_3').innerHTML = `Số nhỏ nhất: ` + min;
}
// Bài 4:
document.getElementById('minPlusNumber').onclick = function() {
    var arrayPlusNumber = [] ;
    for (var i = 0;i <= numberArray.length; i++) {
        if (numberArray[i] > 0) {
            arrayPlusNumber.push(numberArray[i]);
        }
    }
    var minPlus = arrayPlusNumber[0];
    for (var i = 0; i <= arrayPlusNumber.length; i++) {
        if (minPlus > arrayPlusNumber[i]) {
            minPlus = arrayPlusNumber[i];
        } else {
            minPlus = minPlus;
        }
    }
    document.getElementById('result_4').innerHTML = `Số dương nhỏ nhất: ` + minPlus;
}
// Bài 5:
document.getElementById('evenNumber').onclick = function() {
    var evenFinal = 0;
    for (var i = 0; i <= numberArray.length; i++) {
        if (numberArray[i] % 2 == 0) {
            evenFinal = numberArray[i];
        }
    }   
    document.getElementById('result_5').innerHTML = `Số chẵn cuối cùng: ` + evenFinal;
}
// Bài 6:
document.getElementById('change').onclick = function() {
    var viTri1 = document.getElementById('location1').value*1;
    var viTri2 = document.getElementById('location2').value*1;
    var changeViTri = numberArray[viTri1];
    numberArray[viTri1] = numberArray[viTri2];
    numberArray[viTri2] = changeViTri;
    document.getElementById('result_6').innerHTML = numberArray;
}
// Bài 7:
document.getElementById('sort').onclick = function() {
    numberArray.sort(function(a,b) {
        return  a - b;
    });
    document.getElementById('result_7').innerHTML = numberArray;
}
// Bài 8:
document.getElementById('prime').onclick = function() {
    for (var i = 0; i <=numberArray.length; i++) {
        if ((numberArray[i] % 2 != 0 && numberArray[i] % Math.sqrt(numberArray[i])) || numberArray[i] == 2) {
            primeNumber = numberArray[i];
            break;
        } else {
            primeNumber = -1;
        }
    }
    document.getElementById('result_8').innerHTML = primeNumber;
}
// bài 9:
var numberArrayBai9 = [];
document.getElementById('addNumberBai9').onclick = function() {
    var nBai9 = document.getElementById('numberBai9').value*1;
    numberArrayBai9.push(nBai9);
    document.getElementById('arrayBai9').innerHTML = numberArrayBai9;
}
document.getElementById('integer').onclick = function() {
    var integerSum = 0;
    for (var i = 0; i <numberArrayBai9.length;i++) {
        if (Number.isInteger(numberArrayBai9[i])) {
            integerSum ++;
        } else {
            integerSum = integerSum;
        }
    }
    document.getElementById('result_9').innerHTML =`Tổng số nguyên: ` + integerSum;
}
// Bài 10:
document.getElementById('compare').onclick = function() {
    var minus = 0;
    var plus = 0;
    var compareResult = '';
    for (var i = 0; i <= numberArray.length; i++ ) {
        if (numberArray[i] > 0) {
            plus ++;
        }
        if (numberArray[i] < 0) {
            minus ++;
        }
        if (plus > minus) {
        compareResult = 'Số dương > Số âm';
        } else if (plus < minus) {
        compareResult = 'Số âm > Số dương';   
        } else {
        compareResult = 'Số dương = Số âm';
        }   
    }
    document.getElementById('result_10').innerHTML = compareResult;
}